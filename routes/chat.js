var express = require('express');
var router = express.Router();
var db = undefined;

router.get('/', function(req, res, next) {
  db = req.db;
  var collection = db.get('messages');
  collection.find({}, { sort: { $natural: -1 }, limit: 10 }, function(e, data) {
    res.render('chat', {
      title: "Chat",
      messages: data
    });
  })

});

module.exports = {
  router: router,
  socket: function(io) {
    io.sockets.on('connection', function(socket) {
      socket.broadcast.emit('message', 'We got new user!');
      socket.on('message', function(message) {
        socket.broadcast.emit('message', message);
        if(db) db.get('messages').insert({ text: message });
      });
    });
  }
};
